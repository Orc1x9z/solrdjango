-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 17, 2019 at 04:45 PM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 7.3.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `t20190411`
--

-- --------------------------------------------------------

--
-- Table structure for table `auth_group`
--

CREATE TABLE `auth_group` (
  `id` int(11) NOT NULL,
  `name` varchar(80) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `auth_group_permissions`
--

CREATE TABLE `auth_group_permissions` (
  `id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `auth_permission`
--

CREATE TABLE `auth_permission` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `content_type_id` int(11) NOT NULL,
  `codename` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `auth_permission`
--

INSERT INTO `auth_permission` (`id`, `name`, `content_type_id`, `codename`) VALUES
(1, 'Can add log entry', 1, 'add_logentry'),
(2, 'Can change log entry', 1, 'change_logentry'),
(3, 'Can delete log entry', 1, 'delete_logentry'),
(4, 'Can view log entry', 1, 'view_logentry'),
(5, 'Can add permission', 2, 'add_permission'),
(6, 'Can change permission', 2, 'change_permission'),
(7, 'Can delete permission', 2, 'delete_permission'),
(8, 'Can view permission', 2, 'view_permission'),
(9, 'Can add group', 3, 'add_group'),
(10, 'Can change group', 3, 'change_group'),
(11, 'Can delete group', 3, 'delete_group'),
(12, 'Can view group', 3, 'view_group'),
(13, 'Can add user', 4, 'add_user'),
(14, 'Can change user', 4, 'change_user'),
(15, 'Can delete user', 4, 'delete_user'),
(16, 'Can view user', 4, 'view_user'),
(17, 'Can add content type', 5, 'add_contenttype'),
(18, 'Can change content type', 5, 'change_contenttype'),
(19, 'Can delete content type', 5, 'delete_contenttype'),
(20, 'Can view content type', 5, 'view_contenttype'),
(21, 'Can add session', 6, 'add_session'),
(22, 'Can change session', 6, 'change_session'),
(23, 'Can delete session', 6, 'delete_session'),
(24, 'Can view session', 6, 'view_session'),
(25, 'Can add paper data', 7, 'add_paperdata'),
(26, 'Can change paper data', 7, 'change_paperdata'),
(27, 'Can delete paper data', 7, 'delete_paperdata'),
(28, 'Can view paper data', 7, 'view_paperdata');

-- --------------------------------------------------------

--
-- Table structure for table `auth_user`
--

CREATE TABLE `auth_user` (
  `id` int(11) NOT NULL,
  `password` varchar(128) NOT NULL,
  `last_login` datetime(6) DEFAULT NULL,
  `is_superuser` tinyint(1) NOT NULL,
  `username` varchar(150) NOT NULL,
  `first_name` varchar(30) NOT NULL,
  `last_name` varchar(150) NOT NULL,
  `email` varchar(254) NOT NULL,
  `is_staff` tinyint(1) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `date_joined` datetime(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `auth_user_groups`
--

CREATE TABLE `auth_user_groups` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `auth_user_user_permissions`
--

CREATE TABLE `auth_user_user_permissions` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `django_admin_log`
--

CREATE TABLE `django_admin_log` (
  `id` int(11) NOT NULL,
  `action_time` datetime(6) NOT NULL,
  `object_id` longtext,
  `object_repr` varchar(200) NOT NULL,
  `action_flag` smallint(5) UNSIGNED NOT NULL,
  `change_message` longtext NOT NULL,
  `content_type_id` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `django_content_type`
--

CREATE TABLE `django_content_type` (
  `id` int(11) NOT NULL,
  `app_label` varchar(100) NOT NULL,
  `model` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `django_content_type`
--

INSERT INTO `django_content_type` (`id`, `app_label`, `model`) VALUES
(1, 'admin', 'logentry'),
(3, 'auth', 'group'),
(2, 'auth', 'permission'),
(4, 'auth', 'user'),
(5, 'contenttypes', 'contenttype'),
(7, 'paper', 'paperdata'),
(6, 'sessions', 'session');

-- --------------------------------------------------------

--
-- Table structure for table `django_migrations`
--

CREATE TABLE `django_migrations` (
  `id` int(11) NOT NULL,
  `app` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `applied` datetime(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `django_migrations`
--

INSERT INTO `django_migrations` (`id`, `app`, `name`, `applied`) VALUES
(1, 'contenttypes', '0001_initial', '2019-04-15 08:56:34.681128'),
(2, 'auth', '0001_initial', '2019-04-15 08:56:44.173695'),
(3, 'admin', '0001_initial', '2019-04-15 08:56:45.741428'),
(4, 'admin', '0002_logentry_remove_auto_add', '2019-04-15 08:56:45.769371'),
(5, 'admin', '0003_logentry_add_action_flag_choices', '2019-04-15 08:56:45.792360'),
(6, 'contenttypes', '0002_remove_content_type_name', '2019-04-15 08:56:47.336007'),
(7, 'auth', '0002_alter_permission_name_max_length', '2019-04-15 08:56:47.639711'),
(8, 'auth', '0003_alter_user_email_max_length', '2019-04-15 08:56:48.550962'),
(9, 'auth', '0004_alter_user_username_opts', '2019-04-15 08:56:48.573919'),
(10, 'auth', '0005_alter_user_last_login_null', '2019-04-15 08:56:48.721076'),
(11, 'auth', '0006_require_contenttypes_0002', '2019-04-15 08:56:48.744507'),
(12, 'auth', '0007_alter_validators_add_error_messages', '2019-04-15 08:56:48.765451'),
(13, 'auth', '0008_alter_user_username_max_length', '2019-04-15 08:56:49.071875'),
(14, 'auth', '0009_alter_user_last_name_max_length', '2019-04-15 08:56:49.973013'),
(15, 'sessions', '0001_initial', '2019-04-15 08:56:50.166719'),
(16, 'paper', '0001_initial', '2019-04-15 09:01:39.553495');

-- --------------------------------------------------------

--
-- Table structure for table `django_session`
--

CREATE TABLE `django_session` (
  `session_key` varchar(40) NOT NULL,
  `session_data` longtext NOT NULL,
  `expire_date` datetime(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `paper_paperdata`
--

CREATE TABLE `paper_paperdata` (
  `id` int(11) NOT NULL,
  `title` varchar(100) NOT NULL,
  `contents` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `paper_paperdata`
--

INSERT INTO `paper_paperdata` (`id`, `title`, `contents`) VALUES
(47, 'thuyền nguy hiểm lắm', 'Ok Ceyx\n\n(đọc là sex\n\nNếu như bạn đọc đúng)\n\nẢnh cảm thấy bực mình\n\nBởi vì thằng Peleus\n\nNghe được một vài con sói\n\nĂn nguyên đàn bò\n\nỞ trên một ngọn đồi nào đó\n\nThời cổ đại người ta hay tự rước bực vào người bằng những chuyện này\n\nChắc có lẽ hồi xưa k có chính chị.\n\nDù sao thì ceyx quyết định ảnh sẽ đi gặp một thằng tiên tri\n\nVà để gặp được thằng tiên tri này\n\nẢnh phải đi bằng thuyền\n\nVà vợ ảnh Alcyone kiểu ĐỪNG ĐI CHẾT H\n\nVà ceyx kiểu không sao đâu\n\nVà Alcyone lol ok\n\nSau đó ceys leo lên thuyền\n\nVà nó chìm ngay lập tức\n\nsau một ngày sau hay gì đó\n\nNước hóa thành đen và có một vài cơn bão\n\nThề đụ wtf?\n\nMình biết là mấy ông thần làm như vậy\n\nNhưng mà tại sao chứ?\n\nTrong khi mà Alcyone cảm thấy nhớ chồng ở nhà\n\nLúc này đang may đồ hay gì đó\n\nVà đốt nhang cầu nguyện juno\n\nKiểu ê con đàn bà kia trả chồng lại cho tao ok\n\nVà juno kiểu địt cụ\n\nNghe này bitch tao k làm gì cả\n\nChồng mày chết rồi\n\nChết chìm ok\n\nNhưng alcyone k nghe thấy gì đó\n\nBởi vì lúc này ẻm đang may đồ\n\nNên do đó juno gặp con nhỏ này tên là Iris\n\nVà kiểu Iris\n\nĐi gặp thằng hypnos\n\nTHẦN NGỦ\n\nBáo mộng cho alcyone\n\nNói với nó rằng chồng nó chết rồi\n\nVà Iris kiểu sure why the fuck not\n\nSau đó ẻm xuất hiện ở chỗ Hypnos\n\nVà kiểu heyyy\n\nVà ẻm phải hét vào mặt hypnos cả một thế kỷ\n\nĐể ảnh tỉnh dậy\n\nVà ảnh kiểu CÁÁÁÁÁI GÌ\n\nVà Iris kiểu tao muốn mày nói với con này chồng nó chết rồi\n\nVà hypnos đi gặp morpheus\n\nMorpheus là ông thần chuyên giả dạng người khác trong giấc mơ\n\nFuck mình cần năng lực của ông này\n\nDù sao thi Morpheus xuất hiện trong giấc mơ của Alcyone\n\nKiểu hey hey hey ank đêy ceyx đây\n\nChồng em đây\n\nAnh chết rồi\n\nLàm quen với việc đó đi lol\n\nVà sau đó ALcyone tỉnh dậy kiểu waaaaaaah CHỒNG ƠI\n\nFUCK\n\nANH CHẾT RỒI À\n\nLOL OK\n\nVà sau đó sáng hôn sau\n\nẺm đang đi bộ trên bãi biển\n\nVừa đi vừa buồn\n\nVà tự nhiên đâu một cái xác trôi dạt vào bờ biển\n\nVả ẻm kiểu oh địt\n\nĐó có phải là\n\nYeah chồng tao mà\n\nVà ẻm chạy đến cái xác\n\nKhóc lóc các thứ\n\nVả ẻm khóc nhiều đến nỗi\n\nẺm biến thành chim\n\nNhưng mà chim cũng k khiến cho ẻm vui hơn được\n\nVà thực ra\n\nKể cả khi là chim\n\nẺm cũng KHÓC NHIỀU VCL\n\nĐến nỗi mà nước mắt của ẻm biến ceyx thành chim luôn\n\nVà họ chịch và khóc hay là làm gì đó suốt đời'),
(48, 'Alcmene chơi cả hai lỗ', 'Mình nói láo đấy\n\nOkay các bạn à\n\nThành thực mà nói\n\nMình tính kể cho bạn câu chuyện “Amphitryon” của Plautus\n\nNhưng mà nó DÀÀÀÀI vãi cứt\n\nVà mình vẫn phải đi chơi tối nay\n\nNên mình chỉ đọc nửa đầu của câu chuyện thôi\n\nVà phần cuối thì mình đọc tóm tắt\n\nOK\n\nAlcmene là con nhỏ mà chắc có lẽ bạn đã nghe qua\n\nNếu như chưa thì mình sẽ spoil nó ngay bây giờ\n\nĐiều quan trọng là con nhỏ này có một thằng chồng\n\nVà chồng ẻm chính là Amphitryon\n\nVà Amphitryon là đệ của vua Creon.\n\nVào một ngày nọ vua Creon kiểu yo bro\n\nChúng ta sắp chiến tranh\n\nKhông chắc thằng nào\n\nCó lẽ là Teleboans hoặc là Taphians\n\nThằng lồn nào tên bắt đầu bằng chữ T đập nó cho tao lol\n\nVà Amphitryon kiểu YES SIRR\n\nNhưng mà trước khi mà ảnh đi ra chiến trường\n\nẢnh làm một việc khá thông minh đó là khiến con vợ mình có bầu\n\nChắc có lẽ ảnh muốn con mình trả thù cho mình nếu ảnh chết\n\nVà sau đó chiến tranh tới\n\nNhưng mà nghe này, Amphitryon đã mắc một sai lầm\n\n(ở Hy Lạp cổ đại, đó là một sai lầm dễ mắc phải)\n\nAmphitryon đã cưới con vợ mà Zeus muốn chịch sml.\n\nDo đó Zeus thấy Amphitryon đi vào chỗ chết\n\nVà ảnh kiểu ngon!\n\nĐây có lẽ là cơ hội để cắm một tòa tháp đôi vào đầu thằng kia\n\nVà mười phút sau ảnh hóa thân thành chồng của Alcmene\n\nVà ảnh bước vào phòng của ẻm kiểu YO\n\nVà Alcmene kiểu tao tưởng mày ra chiến trường rồi?\n\nVà Zeus kiểu umm\n\n“Cuộc chiến này quá ngắn\n\nCòn cơn nứng của anh dành cho em thì quá dài”\n\nDù sao thì cả hai làm tình\n\nVà trong suốt buổi chịch đó\n\nHermes tội nghiệp của chúng ta phải đứng ở ngoài gác cửa :’(\n\nBởi vì Hermes là\n\nMỘT THẰNG SUPPORT TUYỆT VỜI\n\nVí dụ:\n\nHermes đang đứng gác cửa đúng k?\n\nGiống như việc mà một thằng support nên làm\n\nNhưng sau đó hóa ra Amphytrion thật cũng đang về sớm\n\nVà ảnh gửi tay đưa thư báo với vợ rằng mình sắp về\n\nVà thằng đưa thư chạy một mạch về phòng của Alcmene\n\nVà cuối cùng ảnh nhìn thấy CHÍNH BẢN THÂN MÌNH đang đứng gác cửa\n\nVà ảnh kiểu hey\n\nTao ở đây để báo với con ở trong phòng ck nó đang về ok\n\nVà hermes, lúc này đang giả dạng thành thằng đưa thư\n\nKiểu ĐÉO, THẰNG RÌ TẠC NÀY\n\nMÀY KHÔNG THẤY À?\n\nTAO LÀ MÀY MÀ LOL\n\nMÀY CHẮC CHẮN PHẢI GIẢ DẠNG TAO\n\nBIẾN ĐI, NGƯỜI TA ĐANG CHỊCH TRONG PHÒNG LOL\n\nVà đoán xem?\n\nThằng đưa thư BIẾN THẬT\n\nDĩ nhiên là thằng đưa thư bị tâm thần cmnl\n\nVà quay về gặp Amphitryon để kể lại chuyện xàm loz mình vừa mới gặp\n\nKhiến cho vai chính yêu dấu của chúng ta không còn sự lựa chon nào khác\n\nNgoài việc đích thân đạp cửa phòng vợ minh\n\nVà bước vào.\n\nOkay\n\nMình muốn bạn đặt bản thân mình vào hoàn cảnh đó\n\nBạn vừa có một ngày đi làm/đi học vui vẻ về\n\nVà bạn muốn thư giãn với người bạn yêu\n\nVà bạn bước vào phòng phát hiện ra vợ bạn\n\nLúc này đang có bầu\n\nĐANG LÀM TÌNH VỚI CHÍNH BẢN THÂN BẠN\n\nTRÊN GIƯỜNG CỦA BẠN.\n\nMình đoán là bạn phát điên con mẹ nó luôn\n\nVà đúng là vậy\n\nAmphitryon mất hết khả năng đánh giá thực tại cmnl\n\nVà thằng Zeus còn đổ dầu vô lửa bằng cách khăng khăng khẳng định\n\nmình mới là Amphitryon thật\n\nKiểu lol thằng đéo nào giống anh vậy lol\n\nMặc dù ảnh vẫn có thể làm một cú hit and run cũng được mà\n\nNhưng dù sao thì k ai có thể hiểu được cái tình huống xàm loz này cả\n\nNên do đó họ gọi một người thông thái tên là Blepharo tới\n\nVà đoán xem\n\nBlepharo cũng đéo biết con mẹ gì luôn\n\nÝ mình là ổng phải làm con mẹ gì bây giờ đây?\n\nNên tất cả mọi thứ fuck tạp vcl\n\nOk mình chỉ đọc đến đây thôi\n\nPhần còn lại là mình đọc tóm tắt\n\nSau đó Alcmene giải quyết tình huống đó bằng cách\n\nChọn đúng ngay lúc đó để sinh ra 2 đứa trẻ\n\nVà tia sét maybe\n\nVà khi mà Amphitryon đang còn nằm lăn lộn dưới đất\n\nZeus xuất hiện kiểu dù sao thì các bẹn à\n\nLà mình đấy\n\nCòn bây giờ thì để mình lấy đứa bé này\n\nCho nó bú dzú vợ mình để nó thành immortal\n\nBới vì đoán xem đứa bé đó là ai:\n\nĐÚNG RỒI\n\nTHẰNG LOZ HÉC QUYN ĐẤY\n\nNhưng mà đó là câu chuyện khác\n\nOk bài học của câu chuyện\n\nNếu như bạn muốn ngoại tình\n\nThì nhớ ngoại tình với ai nhìn giống y chang họ vậy\n\nBời vì ít ra bạn còn có thể chối được.'),
(49, 'đây là lý do Hera lại khốn nạn như vậy', 'Ôi lạy hồn\n\nMình sắp bị gia đình đưa đi kiểm tra drug test rồi\n\nNên do đó mình post bài hơi trễ so với mọi tuần\n\nAi có cách pass thì chỉ mình với huhu :’(\n\nNếu không thì chúng ta lâu lắm mới gặp lại đây :(\n\nCác bạn phải giúp mình ok?\n\nCòn bây giờ thì\n\nĐẾN GIỜ THẦN THOẠI RỒI CÁC BẠN À\n\nMình dành cả buổi để nghiên cứu tại sao Hera lại bitchy như vậy\n\nVà nhận thấy\n\nCon này mất dạy vãi cứt\n\nNhưng mà mình không bao giờ để ý\n\nRằng có một LÝ DO cho sự bitchy của bả\n\nVí dụ\n\nZeus chặt cu ông già mình và vứt xuống biển đúng k?\n\nPhần đó thì quá nổi tiếng rồi\n\nVà khá tởm nữa\n\nNhưng mà các bạn thấy đấy điều tiếp theo mà Zeus làm\n\nĐó là ảnh đi nắc con chị sinh đôi của mình\n\nVÂNG\n\nCHÍNH XÁC\n\nHERA LÀ CHỊ CỦA ZEUS\n\nCá là bạn k biết đâu nhỉ\n\nÀ khoan đã đây là thần thoại hy lạp mà\n\nMình quên rằng LOẠN LUÂN LÀ TREND THỊNH HÀNH MÀ\n\nNhưng thật sự là, loạn luân trong trường hợp này không phải ngay lập tức\n\nCác bạn biết đấy Zeus gặp Hera kiểu yo chị\n\nEm nghe đồn rằng chị có cái lỗ vừa với cu em phải k?\n\nEm đến đây để kiểm tra đây\n\nSUỐT ĐÊM LUÔN\n\nVà Hera kiểu Zeus đó là câu pick up line tệ nhất mà tao từng nghe đấy\n\nVà tao lớn lên chung với mày mà\n\nNhớ không\n\nTAO LÀ MÁU MỦ CỦA MÀY MÀ ĐỊT CHỊ MÀY\n\nVà Zeus kiểu “địt chị mày” hehe\n\nVà hera kiểu fuck no lol\n\nNên do đó ảnh quyết định sử dụng trick cũ của mình\n\nĐó là biến thành những con vật dễ thương và sau đó chịch sml người khác\n\nDo đó ổng biến hình thành CON MÈO\n\nÀ nhầm\n\nCon chim cu cu\n\nVà bay đến chỗ Hera và lập tổ ở đó\n\nVà Hera kiểu awwww dễ thương wé\n\nChắc đây không phải là thằng zeus đâu\n\nVà Zeus kiểu BAMMM\n\nNhảy lên người hera\n\nHAHA TAO HIẾP MÀY RỒI GIỜ MÀY PHẢI CƯỚI TAO THÔI\n\nVà Hera kiểu AWWW ĐỊT\n\nDo đó họ đám cưới\n\nVà điều đầu tiên là tuần trăng mật của họ kéo dài BA TRĂM NĂM\n\nĐó không phải là tuần trăng mặt\n\nĐó là THẾ KỶ TRĂNG MẬT\n\nDù sao thì Hera cố gắng giải thoát bản thân ra khỏi cái thể kỷ trăng mật này\n\nĐó là cứ mỗi năm một lần\n\nHera dành nguyên một ngày TẮM SML\n\nẺm tắm nhiều đến nỗi\n\nĐiều đó KHIẾN CHO MÀNG TRINH CỦA ẺM HỒI PHỤC CON MẸ NÓ LUÔN\n\nKhông chỉ là vì ẻm cảm thấy xấu hổ vì cái đám cưới này\n\nMà còn là vì ẻm tự nhiên có bầu mà KHÔNG VÌ LÝ DO GÌ\n\nMột ngày nọ ẻm chạm vào một cành hoa\n\nBAM\n\nSINH RA ARES VÀ ERIS\n\nVào một ngày kia ẻm chạm vào HOA BẮP CẢI\n\nBAM\n\nHEBE RA ĐỜI\n\nNữ thần của tuổi trẻ\n\nVà một ngày kia nữa ẻm ĐÉO CHẠM VÀO THỨ GÌ CẢ\n\nMÀ VẪN SINH RA THẰNG HEPHAETUS\n\nMình thích gọi theo la mã là vulcan hơn bởi vì fuck it why not\n\nVà sau đó vulcan biết được việc này\n\nVà ẢNH TRÓI MẸ MÌNH LẠI VÀO CÁI GHẾ SẮT\n\nCHO ĐẾN KHI ẢNH THỎA MÃN VỚI VIỆC MẸ MÌNH KHÔNG NÓI LÁO VỀ VIỆC TAI SAO MÌNH RA ĐỜI\n\nVà sau 300 năm trăng mật của mình\n\nLà thời kỳ mà hy lạp gọi là\n\n“Thời đại Zeus chịch tất cả vật thể sống bằng cú bắn tinh sấm sét của mình”\n\nVí dụ\n\nổng chịch Themis và sinh ra các mùa và số phận (fate)\n\nổng chịch Mnemosene CHÍNH LẦN và sinh ra lũ thi thần\n\nMẹ của ổng cảm thấy bực mình và cảnh báo ổng rằng ổng chịch quá nhiều\n\nVà Zeus kiểu MẸ À\n\nMẸ MÀ CÒN NÓI ĐẾN CHUYỆN LÀM TÌNH CỦA CON NỮA\n\nKHÔNG LÀ CON BIẾN MẸ THÀNH RẮN VÀ CHỊCH MẸ LUÔN ĐẤY\n\nVà ảnh làm vậy\n\nÀ mà ảnh cũng chơi trò cha con với Persephone\n\nHehe chơi trò cha con\n\nNẾU NHƯ BẠN HIỂU MÌNH MUỐN NÓI GÌ\n\nNên dĩ nhiên là Hera cảm thấy cay cus về việc này\n\nBởi vì các bạn thấy đấy một ngày nọ hera và những vị thần khác cảm thấy phát bệnh vì con cu của Zeus\n\nVà quyết định là sẽ trói ổng lại trong lúc ổng đang ngủ\n\nVà giấu đi tia sét của ổng\n\nNhưng sau đó Zeus tỉnh dậy kiểu CÁI ĐÉO GÌ VẬY LOL\n\nVà tất cả bọn thần kiểu ê mày bị trói rồi\n\nBây giờ thì tụi tao sẽ quyết định xem ai sẽ là vua\n\nĐừng cố gắng chạy thoát ok?\n\nTrong khi mà tụi thần khác đang tranh luận xem ai sẽ thay zeus\n\nThì Thetis lại cảm thấy nhớ mùi cu của Zeus\n\nNhưng mà ẻm không thể chịch Zeus đc bởi vì ổng bị trói mà\n\nVà không phải bị trói theo kiểu sexy đâu\n\nTrói cứng ngắc\n\nHERA TRÓI MÀ\n\nNên do đó Thetis gặp thằng này tên là Briareus\n\nThằng này có CHÍNH XÁC 100 CÁI TAY\n\nVà ảnh đạp cửa phòng vào\n\nCởi trói từng nút\n\nVà sau đó chúng ta không bao giờ nghe tên thằng này nữa\n\nBởi vì ảnh tồn tại chỉ để làm việc này\n\nVà sau đó Zeus kiểu ARRRRG HERA ĐỊT MẸ MÀY\n\nVà trói ẻm lại treo giữa trời\n\nVà tất cả vị thần kiểu ok Zeus tha cho tụi tao, tụi tao sẽ ngoan ok?\n\nDo đó nếu như bạn mới chửi ai là con loz\n\nThì hãy nhớ rằng nó đã có thể bị trói giữa trờI bởi một thằng lon` nào đó\n\nSau 300 năm bị chịch\n\nHoặc có thể là không phải'),
(50, 'Một tấn chất nghi ma túy bị phát hiện tại Nghệ An', 'Nhà chức trách Nghệ An sáng nay đã bắt một số nghi can, thu 7 bao tải tang vật ở huyện Quỳnh Lưu.\n\nPhía sau những \'cung đường ma túy\' xuyên quốc gia\nNguồn tin của VnExpress cho biết, sáng 17/4 Phòng cảnh sát ma túy (Công an Nghệ An) phối hợp với nhiều đơn vị đã phát hiện gần một tấn chất nghi ma túy đá tại địa phận xã Quỳnh Thuận, huyện Quỳnh Lưu.\nÔng Trần Quốc Tuấn (Chủ tịch UBND xã Quỳnh Thuận) cho biết tối 16/4 một  người dân gọi điện thoại phản ánh tới chính quyền về việc thấy 7 bao tải nghi là rác bị đổ trộm xuống lề quốc lộ 48B, cạnh cánh đồng muối. \"Xã không biết bên trong chứa cái gì, đang định tổ chức tiêu hủy thì sáng nay công an đến chuyển đi\", ông Tuấn nói.\n\n\"Khoảng một tấn tang vật bị thu giữ, một số nghi can bị bắt. Cơ quan điều tra đang truy bắt những người liên quan\", nguồn tin nói.\n\nTrước đó ngày 15/4, tại ngôi nhà cấp 4 ở thành phố Vinh, cách xã Quỳnh Thuận 90 km, nhà chức trách đã bắt 5 nghi phạm, thu 600 kg ma túy.\n\nTrong tháng 3, nhà chức trách đã triệt phá đường dây vận chuyển ma túy từ Tam Giác Vàng về Việt Nam rồi xuất ra các nước khác. 1,1 tấn ma túy đá bị thu giữ, 7 người bị bắt. Kẻ cầm đầu được xác định là Wu HeShan (57 tuổi, quốc tịch Trung Quốc), từng ở Việt Nam trong 10 năm, thành lập doanh nghiệp làm bình phong tại quận Bình Tân (TP HCM) để thực hiện các vụ vận chuyển ma túy.'),
(51, 'Giá xăng tiếp tục tăng mạnh', 'Mỗi lít xăng tăng ít nhất hơn 1.100 đồng, các loại dầu cũng tăng hơn 400 đồng, từ 15h chiều nay. \n\nGiá xăng trước áp lực tiếp tục tăng\n\nTheo văn bản của liên Bộ Công Thương - Tài chính, giá các mặt hàng xăng dầu tiếp tục tăng trong kỳ điều hành lúc 15h chiều nay, 17/4.\n\nCụ thể, xăng E5 RON 92 tăng 1.115 đồng mỗi lít; xăng RON 95 thêm 1.202 đồng. Các mặt hàng dầu cũng tăng 291-407 đồng một lít, kg tùy loại. Sau điều chỉnh, giá mỗi lít E5 RON 92 tối đa 19.703 đồng; RON 95 có mặt bằng giá mới 21.235 đồng. Giá dầu hỏa tối đa 16.262 đồng một lít; dầu diesel 17.384 đồng và dầu madut 15.617 đồng một kg.\n\nNgoài mức trích Quỹ bình ổn giá 300 đồng một lít như quy định, liên Bộ cũng tiếp tục xả Quỹ bình ổn 1.456 đồng với mỗi lít xăng E5 RON 92; 743 đồng với RON 95. Các mức chi quỹ với xăng giảm khoảng 560 đồng một lít so với kỳ điều hành cách đây 15 ngày.\nLý do tăng giá xăng và tiếp tục xả Quỹ bình ổn được giải thích là, bình quân giá thành phẩm xăng dầu thế giới trong 15 ngày qua tăng 2,3-4% một thùng.\nNhư vậy đây là đợt tăng giá bán lẻ xăng, dầu liên tiếp thứ hai trong tháng 4, với mức tăng tổng cộng khoảng 2.690 đồng một lít xăng RON 95 và 2.480 đồng với E5 RON92.\n\nTheo số liệu của Tập đoàn Xăng dầu Việt Nam (Petrolimex), Quỹ bình ổn tại doanh nghiệp này đã âm 240 tỷ đồng trước 15h ngày 17/4. Trong khi đó, tại kỳ điều hành chiều nay, nhà chức trách tiếp tục xả Quỹ bình ổn 743-1.456 đồng một lít xăng.\n\n'),
(52, 'Cuộc thi Vietnam Startup Wheel 2019 khởi động', 'Ngoài các startup trong nước, cuộc thi năm nay còn mở rộng ở nhiều quốc gia như Hàn Quốc, Singapore, Nhật Bản...\nSau thành công năm 2018, cuộc thi Vietnam Startup Wheel 2019 tiếp tục phát động trên toàn quốc. Năm nay, cuộc thi dự kiến sẽ thu hút 1.000 dự án khởi nghiệp. Giải Nhất dành cho các tổ chức, cá nhân khởi nghiệp xuất sắc có tổng giá trị lên tới 200 triệu đồng.\n\nVietnam Startup Wheel 2019 sẽ mở rộng quy mô sang Thái Lan và truyền thông mạnh mẽ ở một số quốc gia như: Hàn Quốc, Singapore, Nhật Bản... Ban tổ chức sẽ tuyển chọn hơn 50 dự án khởi nghiệp quốc tế. Đây là điểm mới của cuộc thi nhằm tạo thêm cơ hội giao lưu và học hỏi từ quốc tế cho startup Việt.\n\nHợp tác quốc tế cũng là hoạt động được Vietnam Startup Wheel đẩy mạnh trong năm nay. Đại diện Ban tổ chức - Trung tâm Hỗ trợ Thanh niên Khởi nghiệp BSSC cho biết đang trong quá trình đàm phán với các đối tác quốc tế để cùng xây dựng các chương trình ươm tạo và tăng tốc khởi nghiệp từ 3-6 tháng, dành cho các startup trong từng giai đoạn và từng lĩnh vực.\n\nNgoài ra, các dự án tiêu biểu sẽ được giới thiệu và hỗ trợ trực tiếp tham gia vòng tuyển chọn chương trình Shark Tank Việt Nam 2020.\n\nCuộc thi năm nay cũng sẽ tập trung vào các đối tượng nghiên cứu khoa học. Theo đại diện ban tổ chức, tại Việt Nam, đối tượng này đang gặp vấn đề khó khăn về việc thu hút được các nguồn lực để đưa sản phẩm tới thị trường. \n\nCác dự án nghiên cứu, sáng chế... khi tham gia cuộc thi sẽ có cơ hội đưa sản phẩm của mình đến gần hơn với người tiêu dùng, được hỗ trợ thực hiện quá trình thương mại hóa dự án và nhận nguồn đầu tư.\n\nQuán quân Startup Wheel 2016 - Hồ Đức Hoàn, Founder & CEO Edu2Review - chia sẻ: \"Vietnam Startup Wheel là một cột mốc bước ngoặt đánh dấu một giai đoạn phát triển mới của Ebiv. Chỉ sau 1,5 năm kể từ khi tham gia, Ebiv đã gọi vốn thành công từ một quỹ đầu tư, mở rộng thị trường, nhận cố vấn và thay đổi chiến lược dự án\".\n\nStartup Wheel tiền thân là một cuộc thi Khởi nghiệp tập trung ở TP HCM cho đối tượng thanh niên và sinh viên, do Trung tâm Hỗ trợ Thanh niên Khởi nghiệp (BSSC) phối hợp với Hội Doanh nhân trẻ TP HCM tổ chức từ năm 2013. \n\nNăm 2018, thông qua đề án 844 về Hỗ trợ hệ sinh thái khởi nghiệp đổi mới sáng tạo, cuộc thi mở rộng ra quy mô quốc gia và đổi tên thành Vietnam Startup Wheel.\n\nSau 6 năm tổ chức, cuộc thu thu hút hơn 10.000 thí sinh từ 3.500 dự án tham gia, và hơn 50 tỷ đồng được đầu tư trong và sau cuộc thi.\n\nĐể tìm hiểu thông tin và đăng ký tham gia, truy cập website hoặc fanpage chính thức của cuộc thi. Hạn nộp hồ sơ đến hết 30/4.\n\n'),
(53, 'Hỗ trợ startup năng lượng thông minh tại TP HCM', 'Từ 6 đến 8 startup có ý tưởng tốt lĩnh vực năng lượng sạch, tái tạo, thông minh hoặc tiết kiệm sẽ được hỗ trợ ươm tạo thành doanh nghiệp.\nVườn ươm Doanh nghiệp Công nghệ cao (SHTP-IC) thuộc Khu Công nghệ cao TP HCM và tổ chức phi lợi nhuận New Energy Nexus Đông Nam Á vừa khởi động chương trình New Energy Nexus Việt Nam. \n\nChương trình tập trung hỗ trợ các startup  lĩnh vực công nghệ năng lượng sạch, dự trữ năng lượng, lưới điện thông minh, tiết kiệm năng lượng, quản lý năng lượng, xe điện, tiếp cận năng lượng; cũng như các cải tiến về mô hình kinh doanh, trải nghiệm khách hàng, ứng dụng IOT, số hoá và blockchain trong ngành năng lượng.\n\nChương trình sẽ được triển khai qua 3 giai đoạn. Giai đoạn 1, xây dựng cộng đồng từ tháng 4 đến tháng 6/2019, kêu gọi sự quan tâm và chia sẻ thông tin về lĩnh vực năng lượng cho cộng đồng startup và các nhóm dự án quan tâm đến khởi nghiệp trong lĩnh vực năng lượng.\n\nGiai đoạn 2, tổ chức chương trình Hackathon về chủ đề năng lượng thông minh vào tháng 6/2019). Các nhóm dự án khi tham gia sẽ được đội ngũ mentor hỗ trợ 1-1, mục tiêu sẽ tìm được các dự án tốt tham gia vào giai đoạn 3.\n\nGiai đoạn 3, tiến hành ươm tạo các dự án trong thời gian từ tháng 7/2019 đến 6/2020. Ông Lê Thành Nguyên - Giám đốc SHTP-IC cho biết, chương trình ươm tạo được thiết kế theo mô hình ươm tạo chuyên nghiệp, dành cho 6-8 dự án. Mục tiêu giúp các dự án tham gia chương trình phát triển từ giai đoạn ý tưởng cho đến hình thành doanh nghiệp, phát triển và hoàn thiện sản phẩm, hỗ trợ gia nhập thị trường, kêu gọi vốn từ các quỹ đầu tư.\n\nNew Energy Nexus là tổ chức toàn cầu hỗ trợ các doanh nhân thông qua các quỹ tài chính, chương trình và mạng lưới, phản ánh cho các xu thế mới trong nền kinh tế năng lượng sạch. Mạng lưới của tổ chức bao gồm hơn 100 vườn ươm doanh nghiệp, quỹ tài chính và tổ chức từ 29 quốc gia trên thế giới. \"Mỗi năm chúng tôi có thêm khoảng 5 dự án được hỗ trợ tại từng quốc gia. Các startup xoay quanh cung cấp giải pháp về điện mặt trời trên mái nhà, năng lượng sinh học và năng lượng tái tạo...\", ông Stanley Ng - Giám đốc New Energy Nexus Đông Nam Á cho biết.\n\nÔng Evan Scandling - Đồng lãnh đạo Clean Energy Investment Accelerator tại Việt Nam cho biết, tăng trưởng nhu cầu năng lượng liên tục và xu hướng tìm kiếm các nguồn năng lượng tái tạo, thay thế, thân thiện môi trường là cơ hội lớn cho các startup lĩnh vực năng lượng sạch và thông minh tại Việt Nam.\n\n\"Tăng trưởng GDP cao với dao động 6-7% trong thập kỷ qua và sẽ tiếp tục thời gian tới đồng nghĩa với nhu cầu năng lượng của Việt Nam cũng sẽ tăng 8-10% mỗi năm. Trong thập kỷ tới, đến 2030, thị trường này cần nguồn vốn đầu tư đến 150 tỷ USD cho lĩnh vực năng lượng, bằng cách huy động từ nhiều nguồn khác nhau.\", ông Evan Scandling  nhận định.\n\n'),
(54, 'CEO Alobase: \'Từ chối nhận vốn là một quyết định khó khăn\'', 'Theo CEO nền tảng kết nối kiến trúc sư Alobase.com, vốn là yếu tố quan trọng với startup, nhưng tìm được người đồng hành phù hợp còn quan trọng hơn.\nAlobase là một trong 25 startup nổi bật của chương trình bình chọn Startup Việt 2018 do VnExpress tổ chức.\n\nTop 25 startup được phân bổ vào 5 đội. Mỗi đội có 2-3 chuyên gia đến từ các tổ chức đầu tư, đào tạo và huấn luyện startup phụ trách bồi dưỡng năng lực và phát triển dự án của các nhóm đăng ký thi. Dự kiến vòng đào tạo sẽ diễn ra trong vòng một tháng.\n\nĐộc giả tham gia bình chọn cho top 25 từ nay đến ngày 5/11 tại đây. Kết quả của độc giả được tính một phần vào kết quả chung cuộc.\n\nBan tổ chức sẽ lựa chọn ngẫu nhiên trong danh sách tham gia bình chọn. Giải thưởng là một TV và một smartphone cho duy nhất một độc giả. Bạn đọc may mắn sẽ nhận giải tại lễ vinh danh Startup Việt 2018 diễn ra vào 15/11.\n\nChương trình bình chọn Startup Việt 2018 nhằm kết nối, ươm mầm, tìm kiếm các startup nổi bật trong nhiều lĩnh vực tại Việt Nam; góp phần thúc đẩy tinh thần sáng tạo khởi nghiệp, vinh danh những mô hình kinh doanh đột phá, phát triển bền vững và hữu ích về kinh tế - xã hội...\n\n'),
(55, 'Startup kết nối người mua và chuỗi thương hiệu gần nhất', 'Gannha.com là nền tảng tích hợp kết nối người dùng với điểm bán thương hiệu chuỗi gần nhất theo thời gian thực do CEO Nguyễn Trung Khánh nghiên cứu và phát triển từ năm 2016.\nHiện gannha.com phát hành thử nghiệm theo dạng ứng dụng tích hợp trên nền tảng Android và iOS. Trong ít nhất ba thao tác chạm trên smartphone, người dùng sẽ được kết nối ngay đến vị trí gần nhất của trên 1.300 thương hiệu chuỗi như: cửa hàng tiện lợi, nhà thuốc, điểm gửi xe ô tô, dự án căn hộ, sự kiện giải trí - làm đẹp và du lịch ...\n\nỨng dụng có các chức năng vượt trội như: hiển thị các chương trình bán hàng, tuyển & ứng tuyển nhân viên, hiển thị các dịch vụ tại điểm bán cùng các ưu đãi vừa được đăng lên tức thì. Cũng từ gannha.com, người dùng có thể lập tức tạo cuộc hẹn gặp riêng với bạn tại thương hiệu yêu thích, nhận và hồi đáp lời mời cuộc hẹn dễ dàng và ngay tức thì.\n\nVới kinh nghiệm tư vấn và thiết kế thương hiệu hơn 15 năm, Anh Nguyễn Trung Khánh cho biết, sứ mệnh của marketing là \"kết nối thành công thương hiệu chính chủ đến với nhu cầu của người dùng một cách tức thì\". Gannha.com được sinh ra dựa trên mối quan hệ bền chặt giữa nhu cầu của con người và những dịch vụ, thương hiệu gần với họ nhất, từ đó giúp họ tối ưu thời gian và giá trị sống. CEO của gannha.com chọn một cái tên thuần Việt, bởi một thương hiệu có âm ngữ dễ đọc, thân thương, dễ nhận biết sẽ nhanh chóng chạm đến nhu cầu tức thì mỗi ngày của hàng triệu người Việt.\n\nKhi xây dựng hệ nền tảng định vị thương hiệu theo thời gian thực của gannha.com, anh Khánh cho rằng: “Các ứng dụng giúp tìm địa điểm, tìm giảm giá, khuyến mãi, tích điểm, hoàn tiền chưa đủ để tạo kết nối bền vững với khách hàng. Thay vào đó, hãy mang đến các giá trị về thời gian và chất lượng sản phẩm chính chủ đến họ\". Bên cạnh đó, anh cũng nhận thấy những bối rối của người dùng trong các ứng dụng tìm kiếm ngày càng có nhiều thông tin sai lệch, nhàm chán, cùng với những dịch vụ đi kèm kém chất lượng.\n\nTrong khi đó, bất cứ ai cũng có nhu cầu thiết lập những buổi hẹn. Anh lấy triết lý này để xây dựng yếu tố \"gần\". Anh dùng từ \"gần\" đặt tên cho sản phẩm của mình để giúp người dùng cảm nhận giá trị của thời gian. Bên cạnh đó, Gần Nhà còn là kênh phân phối thông tin chính thức từ các điểm bán đến người dùng, giúp các dịch vụ tối ưu hóa lợi ích dến người dùng.\n\nThay cho những địa điểm đơn lẻ, gannha.com sẽ hiển thị những dịch vụ và cửa hiệu đã phát triển thành hệ thống chuỗi thuộc năm lĩnh vực: bán lẻ - nhà ở - việc làm, học hành, vận tải, vận chuyển - giải trí, làm đẹp và du lịch. Điều này minh chứng cho tiềm năng mở rộng nền tảng, đáp ứng nhu cầu ngày một đa dạng và tích hợp của người dùng.\n\nGannha.com còn là kênh quảng cáo kiểu mới từ các điểm bán của thương hiệu chính chủ đến người dùng gần nhất, giúp các họ thực hiện các chiến dịch kéo - đẩy marketing đạt đến hiệu suất chuyển đổi giá trị khách hàng là tối ưu nhất.\n\nĐể hiện thực hóa tầm nhìn, anh Khánh coi trọng quá trình xây dựng đội ngũ, bên cạnh phát triển sản phẩm. Hiện tại, công ty có tám nhóm hoạt động song song, đội ngũ trẻ với hầu hết các bạn đều thuộc thế hệ 9x.\n\n\"Gannha.com có bốn nguyên tắc mà bất cứ nhân viên nào cũng phải thấm nhuần: hãy làm những điều tốt đẹp, phải kiên trì, hãy am hiểu quá trình và hãy tận hưởng công việc mà mình yêu thích\", anh Khánh nhấn mạnh\n\nTừng làm chủ một công ty làm đẹp thương hiệu, anh Khánh cho biết, trước đây anh luôn quản trị con người theo hướng phát triển năng lực sáng tạo vượt trội, nỗ lực tạo cho cho nhân viên cơ hội để thực hiện tốt nhất ý tưởng của họ.\n\nTrong quá trình triển khai gannha.com, anh Khánh nhận thấy CEO của các tập đoàn lớn đã thuyết phục, duy trì người tài giỏi cùng làm việc với doanh nghiệp bằng cách truyền cho họ những năng lượng tích cực và tầm nhìn bằng hành động của chính mình. Anh Khánh từ đó nhiệt tình tham gia các cuộc thi, hoạt động dành cho doanh nghiệp, vừa để quảng bá cho sản phẩm của doanh nghiệp, vừa để cả công ty từ cấp quản trị đến nhân viên đều có cơ hội đồng hành và cảm nhận sự nhiệt huyết của nhau, hướng đến một mục tiêu chung của doanh nghiệp.\n\n\"Các bạn thấy người làm chủ có hành động, có nỗ lực chứ không phải ngồi một chỗ, điều đó sẽ giữ chân các bạn . Tỷ lệ chuyển - nghỉ việc ở gannha.com duy trì ở mức 3-5%\", anh Khánh chia sẻ.\n\nTại công ty, anh thực hiện nguyên tắc 6-12-2, tức 6 tháng sẽ xem xét hiệu quả công việc chính thức, 12 tháng điều chỉnh mức lương một lần, và hai năm thì thăng bậc hoặc chuyển vị trí công tác. Đây là lộ trình cho các bạn tự khẳng định năng lực bản thân, sau hai năm phải lên cấp quản lý để truyền lửa cho các bạn trẻ hơn.\n\nBảo An\n\nGannha.com là một trong 25 startup nổi bật của chương trình bình chọn Startup Việt 2018 do VnExpress tổ chức.\n\nTop 25 startup được phân bổ vào 5 đội. Mỗi đội có 2-3 chuyên gia đến từ các tổ chức đầu tư, đào tạo và huấn luyện startup phụ trách bồi dưỡng năng lực và phát triển dự án của các nhóm đăng ký thi. Dự kiến vòng đào tạo sẽ diễn ra trong vòng một tháng.\n\nĐộc giả tham gia bình chọn cho top 25 từ nay đến ngày 5/11 tại đây. Kết quả của độc giả được tính một phần vào kết quả chung cuộc.\n\nBan tổ chức sẽ lựa chọn ngẫu nhiên trong danh sách tham gia bình chọn. Giải thưởng là một TV và một smartphone cho độc giả duy nhất. Bạn đọc may mắn sẽ nhận giải tại lễ vinh danh Startup Việt 2018 diễn ra vào 15/11.\n\nChương trình bình chọn Startup Việt 2018 nhằm kết nối, ươm mầm, tìm kiếm các startup nổi bật trong nhiều lĩnh vực tại Việt Nam; góp phần thúc đẩy tinh thần sáng tạo khởi nghiệp, vinh danh những mô hình kinh doanh đột phá, phát triển bền vững và hữu ích về kinh tế - xã hội...\n\n'),
(56, 'Một số hãng công nghệ quan tâm đến AI và IoT hơn blockchain', 'Nhiều công ty công nghệ lớn trên thế giới ưu tiên cho các chiến dịch chuyển đổi về trí tuệ nhân tạo, Internet vạn vật hơn là blockchain.\nBlockchain là một trong những từ khóa được quan tâm rộng rãi trong vài năm trở lại đây. Tuy nhiên, theo một nghiên cứu mới nhất từ nhà cung cấp phần mềm đám mây quản lý về nhận diện Okta của Mỹ, các hãng công nghệ đang đầu tư nhiều vào trí tuệ nhân tạo (AI) và Internet của vạn vật (IoT) hơn là blockchain.\n\nKhảo sát đặt câu hỏi với những người quyết định về mảng công nghệ thông tin, bảo mật và kỹ thuật của các công ty có giá trị trên 1 tỷ USD. Câu hỏi là \"liệu công ty của bạn có đầu tư vào các mảng trí tuệ nhân tạo, thực tế ảo, blockchain hay Internet của vạn vật như một phần trong chiến lược chuyển đổi số\".\n\nCâu hỏi được khảo sát trong tháng một và hai năm nay. 90% người tham gia cho biết họ đang đầu tư ít nhất một trong các công nghệ trên như là trọng tâm của chiến dịch chuyển đổi số. Trong 90% ấy, 61% trả lời có đầu tư vào blockchain, trong khi con số với IoT và AI lần lượt là 72% và 68%. Sâu hơn, kết quả cho thấy blockchain vẫn là một cách để vận hành trước khi trở thành một bước đi chiến lược trong kế hoạch phát triển của các doanh nghiệp này.\n\nVào cuối tháng 3 vừa qua, website của Facebook đăng tải trên 20 công việc liên quan đến blockchain. Đầu tuần này, Ủy ban chứng khoán và sàn giao dịch Mỹ đăng tải công việc \"chuyên viên mật mã\". \n\nTheo Indeed.com, trong thời gian từ tháng 2/2018 đến nay, số lượng công việc liên quan đến blockchain, bitcoin và mật mã đã tăng đến 90%.'),
(57, 'Quỹ đầu tư 100 triệu USD của Starbucks', 'Quỹ đầu tư sẽ huy động thêm 300 triệu USD để đồng hành cùng các startup trong mảng ăn uống và bán lẻ.\nQuỹ đầu tư của Starbucks được quản lý bởi Valor Equity Partners, tập trung vào các startup trong ngành thực phẩm và bán lẻ. Valor Equity là nhà đầu tư giai đoạn đầu của Tesla và nhiều startup nổi tiếng khác.\n\nQuỹ Valor Siren hướng tới mục tiêu huy động thêm khoảng 300 triệu USD nữa trong những tháng tới từ các nhà đầu tư khác. CEO Kevin Johnson cho biết Starbucks có thể khám phá các mối quan hệ thương mại trên con đường đồng hành với các startup mà công ty đầu tư.\n\nNhư vậy, Starbucks là cái tên mới nhất trong ngành ăn uống ở Mỹ đầu tư vào startup. Thông qua quỹ đầu tư Valor Siren, chuỗi cà phê lớn nhất thế giới gia nhập hàng ngũ các công ty thực phẩm lớn chuyển việc tìm kiếm ý tưởng mới thông qua các nguồn lực từ bên ngoài.\n\nVào 2016, Tyson Foods - nhà sản xuất thịt lớn nhất Mỹ cũng thành lập quỹ Tyson để đầu tư vào các công ty tập trung phát triển protein từ thực vật. Trong khi đó, nhiều thương hiệu đồ ăn và thức uống khác cũng gia nhập cuộc chơi, bao gồm cả Kraft Heinz và PepsiCo.\n\nStarbucks - một trong những chuỗi nhà hàng lớn nhất thế giới đang đối mặt với thực tế là họ không thể tạo ra doanh số tăng trưởng nhanh chóng so với kỳ vọng của các nhà đầu tư.\n\nChuỗi cà phê cũng thông báo sẽ bắt đầu hiện đại hóa cơ sở hạ tầng với các định dạng mới vào mùa hè này tại New York. Giám đốc vận hành Roz Brewer cho biết Starbucks muốn cải tổ phong cách \"thế giới thứ ba\" mà công ty là người tiên phong. Ý tưởng mang đến một không gian thư giãn nhưng không phải là nhà hay cơ quan là một trong những mục tiêu mà các đối thủ cạnh tranh của hãng đang ngày một nhân rộng trên thị trường.\n\nVới các cổ đông, Starbucks cũng đang bắt đầu chương trình mua lại cổ phiếu như một phần trong mục tiêu trả lại 25 tỷ USD cho cổ đông trong thời gian ba năm. Năm ngoái, cổ phiếu Starbucks tăng 11% giá trị.\n\n'),
(58, 'Việt Nam tổ chức hội nghị về khởi nghiệp châu Á - Thái Bình Dương', '36 đội thi đến từ 25 quốc gia và vùng lãnh thổ cùng tranh tài trong sự kiện Youth Co:Lab Summit 2019.\nHội nghị thanh niên khởi nghiệp sáng tạo xã hội khu vực châu Á (Youth Co:Lab Summit) diễn ra từ 2-5/4 tại ĐH Kinh tế Quốc dân, Hà Nội, do Trung tâm Đổi mới Khu vực Châu Á Thái Bình Dương của Chương trình phát triển Liên hợp quốc (UNDP) phối hợp với Youth Co:Lab tổ chức.\n\nTheo thống kê của Youth Co:Lab, có khoảng 700 triệu thanh niên ở khu vực châu Á - Thái Bình Dương có khả năng quyết định tương lai khu vực. Họ cần được tiếp cận các chính sách giáo dục, tài chính và nhận được các khoản đầu tư, hỗ trợ để phát huy tiềm năng đó.\n\nĐồng sáng lập bởi UNDP và tổ chức Citi Foundation, Youth Co:Lab mục tiêu thiết lập chương trình chung cho các quốc gia khu vực châu Á - Thái Bình Dương để trao quyền và đầu tư vào lớp trẻ, giúp họ thực hiện mục tiêu phát triển bền vững thông qua khả năng lãnh đạo, hoạt động đổi mới sáng tạo và khởi nghiệp.\n\nTheo đại diện ban tổ chức, sự kiện dự kiến thu hút hơn 400 cá nhân tham dự trên hơn 25 quốc gia khu vực châu Á - Thái Bình Dương, bao gồm doanh nhân trẻ, người làm chính sách cho khởi nghiệp sáng tạo, khu vực tư nhân, nhà đầu tư, chuyên gia và các quan chức cấp cao từ Liên Hợp Quốc.\n\nTại Hội nghị thanh niên khởi nghiệp sáng tạo xã hội khu vực châu Á - Thái Bình Dương năm nay, Việt Nam có hai đại diện tham gia là Vulcan Augmetics và Fly.\n\nVulcan Augmetics là mô hình khởi nghiệp chuyên sản xuất các module chân tay giả giá thấp dành cho người khuyết tật, sử dụng công nghệ in 3D. Startup mục tiêu giúp 38 triệu người khuyết tật thay đổi cuộc sống và có việc làm ổn định. Vừa qua, Vulcan Augmetics là quán quân giải thưởng doanh nhân cộng đồng Blue Venture Award.\nNhóm Fly cung cấp ứng dụng học ngôn ngữ ký hiệu trên smartphone. Ứng dụng này bao gồm các bài học, trò chơi giáo dục và chức năng trò chuyện với giao diện thân thiện, dễ sử dụng. Với công cụ này, nhóm mong muốn ngôn ngữ ký hiệu sẽ đến gần hơn với nhiều gia đình, giúp cải thiện giao tiếp của cộng đồng khiếm thính.\n\n\n'),
(59, 'CEO Alobase: \'Từ chối nhận vốn là một quyết định khó khăn\'', 'Theo CEO nền tảng kết nối kiến trúc sư Alobase.com, vốn là yếu tố quan trọng với startup, nhưng tìm được người đồng hành phù hợp còn quan trọng hơn.\nAlobase là một trong 25 startup nổi bật của chương trình bình chọn Startup Việt 2018 do VnExpress tổ chức.\n\nTop 25 startup được phân bổ vào 5 đội. Mỗi đội có 2-3 chuyên gia đến từ các tổ chức đầu tư, đào tạo và huấn luyện startup phụ trách bồi dưỡng năng lực và phát triển dự án của các nhóm đăng ký thi. Dự kiến vòng đào tạo sẽ diễn ra trong vòng một tháng.\n\nĐộc giả tham gia bình chọn cho top 25 từ nay đến ngày 5/11 tại đây. Kết quả của độc giả được tính một phần vào kết quả chung cuộc.\n\nBan tổ chức sẽ lựa chọn ngẫu nhiên trong danh sách tham gia bình chọn. Giải thưởng là một TV và một smartphone cho duy nhất một độc giả. Bạn đọc may mắn sẽ nhận giải tại lễ vinh danh Startup Việt 2018 diễn ra vào 15/11.\n\nChương trình bình chọn Startup Việt 2018 nhằm kết nối, ươm mầm, tìm kiếm các startup nổi bật trong nhiều lĩnh vực tại Việt Nam; góp phần thúc đẩy tinh thần sáng tạo khởi nghiệp, vinh danh những mô hình kinh doanh đột phá, phát triển bền vững và hữu ích về kinh tế - xã hội...\n\n'),
(60, 'TP HCM tổ chức giải thưởng về sáng tạo, khởi nghiệp năm 2019', 'Giải thưởng I-Star 2019 của TP HCM vừa khởi động nhằm vinh danh các ý tưởng đổi mới sáng tạo và khởi nghiệp.\nSở Khoa học và Công nghệ TP HCM vừa công bố \'Giải thưởng Đổi mới sáng tạo và khởi nghiệp thành phố năm 2019\' (I-Star 2019). Đây là năm thứ hai I-Star được tổ chức nhằm tôn vinh các tổ chức, cá nhân tiêu biểu trong hoạt động đổi mới sáng tạo và khởi nghiệp.\n\nGiải năm nay dành cho 4 đối tượng gồm doanh nghiệp khởi nghiệp, giải pháp đổi mới sáng tạo, tác phẩm truyền thông về khởi nghiệp đổi mới sáng tạo và các tổ chức cá nhân có hỗ trợ cho hoạt động khởi nghiệp đổi mới sáng tạo như vườn ươm doanh nghiệp, nhà đầu tư thiên thần, chuyên gia tư vấn...\n\nTrong đó, doanh nghiệp khởi nghiệp phải là doanh nghiệp nhỏ, vừa được thành lập để thực hiện ý tưởng kinh doanh trên cơ sở khai thác tài sản trí tuệ, công nghệ, mô hình kinh doanh mới và có khả năng tăng trưởng nhanh. Tiêu chí xét chọn dựa trên tính sáng tạo, hiệu quả kinh tế và tác động xã hội.\n\nI-Star năm nay sẽ có tổng cộng 12 giải được trao, với mỗi nhóm đối tượng được chọn trao 3 giải đồng hạng. Ban tổ chức nhận hồ sơ tham dự đến hết tháng 8/2019 và tiến hành công bố công khai trên website để cộng đồng bình chọn, song song với việc xét giải của hội đồng chuyên môn. Giải sẽ được trao vào tháng 10/2019.\n\nTrong lần đầu diễn ra năm ngoái, ban tổ chức I-Star đã nhận được gần 200 bài dự thi. Kết quả, 11 bài xuất sắc nhất đã giành giải thưởng với mỗi giải trị giá 50 triệu đồng và được vinh danh trong khuôn khổ \'Tuần lễ Đổi mới sáng tạo và Khởi nghiệp TPHCM 2018\'.\n\n'),
(61, 'Cơ hội nhận hỗ trợ kinh phí cho các đóng góp khởi nghiệp', 'Các đơn vị được lựa chọn sẽ được hỗ trợ kinh phí, thực hiện các hoạt động thúc đẩy khởi nghiệp từ Bộ Khoa học và Công nghệ.\nĐề án Hỗ trợ hệ sinh thái khởi nghiệp đổi mới sáng tạo quốc gia (Đề án 844) là một trong những đề án về khởi nghiệp đầu tiên ở Việt Nam, do Bộ Khoa học và Công nghệ chủ trì. Từ năm 2017, qua 3 năm thực hiện, đề án dự kiến đã và sẽ hỗ trợ kinh phí cho 63 dự án từ gần 30 tổ chức hỗ trợ khởi nghiệp trên toàn quốc.\n\nNăm nay, Bộ Khoa học và Công nghệ tiếp tục tổ chức các hoạt động nhằm hỗ trợ Hệ sinh thái khởi nghiệp sáng tạo ở Việt Nam. Đây là cơ hội để các đơn vị, tổ chức khởi nghiệp có năng lực, kinh nghiệm được hỗ trợ kinh phí để thực hiện các hoạt động đã đề xuất nhằm thúc đẩy phong trào khởi nghiệp tại Việt Nam. \n\nCụ thể, nội dung đề xuất sẽ bao gồm tính cấp thiết, đề xuất nội dung nhiệm vụ, cũng như giải trình nguồn lực thực hiện nhiệm vụ đó nhằm giải quyết các vấn đề của Hệ sinh thái khởi nghiệp sáng tạo. Các nhiệm vụ do đơn vị nộp đề xuất tới Bộ Khoa học và Công nghệ cần bám sát chức năng, nhiệm vụ, thế mạnh về đội ngũ chuyên gia của đơn vị, nhu cầu thực tiễn của địa phương. \n\nTrong đó, nội dung đề xuất liên quan tới các vấn đề như hoàn thiện cơ chế chính sách; đào tạo, nâng cao năng lực; cung cấp dịch vụ cho khởi nghiệp; hỗ trợ và liên kết các thành phần thuộc hệ sinh thái; truyền thông phát triển văn hóa khởi nghiệp.\n\nLà một trong những đơn vị tham gia Đề án trong suốt 3 năm qua, ông Phạm Tuấn Hiệp - Giám đốc ươm tạo Công ty TNHH MTV Đầu tư và phát triển công nghệ Bách khoa Hà Nội (BK-Holdings) chia sẻ: \"Thông qua Đề án 844, BK-Holdings đã có thêm nhiều kết nối, nhiều đối tác trong hệ sinh thái khởi nghiệp, có cơ hội hợp tác liên trường, liên ngành trong lĩnh vực ươm tạo, sáng tạo công nghệ và thương mại hóa công nghệ. BK-Holdings cũng trở thành đối tác triển khai Đề án 1665 của Bộ Giáo dục và Đào tạo, đối tác xây dựng Đề án Khởi nghiệp sáng tạo của Hà Nội\".\n\nÔng Phạm Dũng Nam, Giám đốc Văn phòng Đề án 844 đánh giá về các đơn vị tham gia đề án: \"Các đơn vị tham gia thực hiện Đề án 844 đều là những tổ chức có nhiều năm kinh nghiệm trong việc đầu tư, cố vấn, đào tạo cho khởi nghiệp, đã từng hỗ trợ nhiều doanh nghiệp khởi nghiệp tiềm năng gọi vốn thành công. Với mô hình triển khai như hiện tại của Đề án 844, nguồn Ngân sách của nhà nước có thể được sử dụng một cách hiệu quả, phù hợp với nhu cầu của xã hội và là tiền đề cho sự phát triển lớn mạnh hơn của toàn bộ hệ sinh thái nước nhà\".Từ ngày 25/2 đến 25/3, Bộ Khoa học và Công nghệ thông báo kêu gọi các đơn vị, tổ chức tham gia đề xuất nhiệm vụ hàng năm, định kỳ thuộc Đề án 844 thực hiện từ năm 2020. Danh mục nhiệm vụ được phê duyệt cũng như thông báo tuyển chọn tổ chức chủ trì thực hiện nhiệm vụ dự kiến công bố vào tháng 4/2019.\n\nĐể biết thêm thông tin chi tiết về cách thức đăng ký nộp đề xuất nhiệm vụ, các đơn vị theo dõi thông báo tại đây\n\n');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `auth_group`
--
ALTER TABLE `auth_group`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Indexes for table `auth_group_permissions`
--
ALTER TABLE `auth_group_permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `auth_group_permissions_group_id_permission_id_0cd325b0_uniq` (`group_id`,`permission_id`),
  ADD KEY `auth_group_permissio_permission_id_84c5c92e_fk_auth_perm` (`permission_id`);

--
-- Indexes for table `auth_permission`
--
ALTER TABLE `auth_permission`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `auth_permission_content_type_id_codename_01ab375a_uniq` (`content_type_id`,`codename`);

--
-- Indexes for table `auth_user`
--
ALTER TABLE `auth_user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`);

--
-- Indexes for table `auth_user_groups`
--
ALTER TABLE `auth_user_groups`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `auth_user_groups_user_id_group_id_94350c0c_uniq` (`user_id`,`group_id`),
  ADD KEY `auth_user_groups_group_id_97559544_fk_auth_group_id` (`group_id`);

--
-- Indexes for table `auth_user_user_permissions`
--
ALTER TABLE `auth_user_user_permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `auth_user_user_permissions_user_id_permission_id_14a6b632_uniq` (`user_id`,`permission_id`),
  ADD KEY `auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm` (`permission_id`);

--
-- Indexes for table `django_admin_log`
--
ALTER TABLE `django_admin_log`
  ADD PRIMARY KEY (`id`),
  ADD KEY `django_admin_log_content_type_id_c4bce8eb_fk_django_co` (`content_type_id`),
  ADD KEY `django_admin_log_user_id_c564eba6_fk_auth_user_id` (`user_id`);

--
-- Indexes for table `django_content_type`
--
ALTER TABLE `django_content_type`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `django_content_type_app_label_model_76bd3d3b_uniq` (`app_label`,`model`);

--
-- Indexes for table `django_migrations`
--
ALTER TABLE `django_migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `django_session`
--
ALTER TABLE `django_session`
  ADD PRIMARY KEY (`session_key`),
  ADD KEY `django_session_expire_date_a5c62663` (`expire_date`);

--
-- Indexes for table `paper_paperdata`
--
ALTER TABLE `paper_paperdata`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `auth_group`
--
ALTER TABLE `auth_group`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `auth_group_permissions`
--
ALTER TABLE `auth_group_permissions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `auth_permission`
--
ALTER TABLE `auth_permission`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT for table `auth_user`
--
ALTER TABLE `auth_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `auth_user_groups`
--
ALTER TABLE `auth_user_groups`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `auth_user_user_permissions`
--
ALTER TABLE `auth_user_user_permissions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `django_admin_log`
--
ALTER TABLE `django_admin_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `django_content_type`
--
ALTER TABLE `django_content_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `django_migrations`
--
ALTER TABLE `django_migrations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `paper_paperdata`
--
ALTER TABLE `paper_paperdata`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=62;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `auth_group_permissions`
--
ALTER TABLE `auth_group_permissions`
  ADD CONSTRAINT `auth_group_permissio_permission_id_84c5c92e_fk_auth_perm` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  ADD CONSTRAINT `auth_group_permissions_group_id_b120cbf9_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`);

--
-- Constraints for table `auth_permission`
--
ALTER TABLE `auth_permission`
  ADD CONSTRAINT `auth_permission_content_type_id_2f476e4b_fk_django_co` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`);

--
-- Constraints for table `auth_user_groups`
--
ALTER TABLE `auth_user_groups`
  ADD CONSTRAINT `auth_user_groups_group_id_97559544_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`),
  ADD CONSTRAINT `auth_user_groups_user_id_6a12ed8b_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`);

--
-- Constraints for table `auth_user_user_permissions`
--
ALTER TABLE `auth_user_user_permissions`
  ADD CONSTRAINT `auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  ADD CONSTRAINT `auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`);

--
-- Constraints for table `django_admin_log`
--
ALTER TABLE `django_admin_log`
  ADD CONSTRAINT `django_admin_log_content_type_id_c4bce8eb_fk_django_co` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`),
  ADD CONSTRAINT `django_admin_log_user_id_c564eba6_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
