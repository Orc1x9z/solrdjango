from django.urls import path, include
from paper import views
from django.conf.urls import url

urlpatterns = [
    path('search/', include('haystack.urls')),
    path('paper/', views.paper_list),
    path('paper/<int:pk>/', views.paper_detail),
    path('paper/search/', views.search_paper),
    # path('paper/search/', include('haystack.urls')),
]