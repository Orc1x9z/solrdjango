from haystack import indexes
from paper.models import PaperData


class PaperIndex(indexes.SearchIndex, indexes.Indexable):
    text = indexes.CharField(document=True, use_template=False, template_name="paper_text.txt")
    id = indexes.IntegerField(model_attr='id')
    title = indexes.CharField(model_attr='title')
    contents = indexes.CharField(model_attr='contents')

    def get_model(self):
        return PaperData

    def index_queryset(self, using=None):
        """Used when the entire index for model is updated."""
        return self.get_model().objects.all()
