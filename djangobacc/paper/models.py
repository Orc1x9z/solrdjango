from django.db import models

# Create your models here.
class PaperData(models.Model):
    title = models.CharField(max_length=100)
    contents = models.TextField()

    class Meta:
        ordering = ('id',)