from __future__ import print_function
from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from django.http import HttpResponse, JsonResponse
from rest_framework.renderers import JSONRenderer
from rest_framework.parsers import JSONParser
from paper.models import PaperData
from paper.serializers import PaperDataSerializer
from django.conf import settings
from django.core import serializers
import json
import pysolr
from haystack.query import SearchQuerySet, SQ
from haystack.utils import Highlighter
import re


@csrf_exempt
def paper_list(request):
    """
    List all papers, or create a new paper.
    """
    solr = pysolr.Solr(
        settings.HAYSTACK_CONNECTIONS['default']['URL'], timeout=10)
    if request.method == 'GET':
        papers = PaperData.objects.all()
        # serializer = PaperDataSerializer(papers, many=True)
        data = list(papers.values())
        return JsonResponse(data, safe=False)

    elif request.method == 'POST':
        data = JSONParser().parse(request)
        # serializer = PaperDataSerializer(data=data)
        new_paper = PaperData.objects.create(
            title=data['title'],
            contents=data['contents']
        )
        new_paper.save()

        # solr.add([
        #     # {
        #     #     "id": 9,
        #     #     "title": "asdasdasdasd",
        #     #     "contents": "czxczxczxczxc"
        #     # },
        #     {
        #         "id": new_paper.id + 100000,
        #         "title": no_accent_vietnamese(data['title']) ,
        #         "contents": no_accent_vietnamese(data['contents']),
        #         "django_ct":"paper.paperdata",
        #         "django_id":new_paper.id + 100000,
        #     },
        # ])
        # solr.commit()
        return JsonResponse({'success': True})


@csrf_exempt
def paper_detail(request, pk):
    """
    Retrieve, update or delete a paper.
    """
    solr = pysolr.Solr(
        settings.HAYSTACK_CONNECTIONS['default']['URL'], timeout=10)
    try:
        paper = PaperData.objects.get(pk=pk)
    except PaperData.DoesNotExist:
        return HttpResponse(status=404)

    if request.method == 'GET':
        serializer = PaperDataSerializer(paper)
        return JsonResponse(serializer.data)

    elif request.method == 'PUT':
        data = JSONParser().parse(request)
        serializer = PaperDataSerializer(paper, data=data)
        if serializer.is_valid():
            serializer.save()
            return JsonResponse(serializer.data)
        return JsonResponse(serializer.errors, status=400)

    elif request.method == 'DELETE':
        paper.delete()
        solr.delete(id=[pk])
        solr.commit()
        # solr.delete(q='*:*')
        return JsonResponse({'success': True})


@csrf_exempt
def search_paper(request):
    solr = pysolr.Solr(
        settings.HAYSTACK_CONNECTIONS['default']['URL'], timeout=10)
    data = JSONParser().parse(request)
    search_string = data['search_string']

    results = solr.search('title:'+search_string+' OR contents:'+search_string, **{
        # "start":"10",
        # "rows":"10",
        "hl":"on",
        "hl.fl":"title,contents",
        "hl.simple.pre":"<mark>",
        "hl.simple.post":"</mark>",
        'hl.fragsize': 200,
    })

    for result in results:
        # print( results.highlighting[result['id']])
        if('title' in results.highlighting[result['id']]):
            result['title'] = results.highlighting[result['id']]['title'][0]
        if('contents' in results.highlighting[result['id']]):
            result['contents'] = results.highlighting[result['id']]['contents'][0]
    
    serializer_result = PaperDataSerializer(results, many=True)
    return JsonResponse(serializer_result.data, safe=False)


def no_accent_vietnamese(s):
    # s = s.decode('utf-8')
    s = re.sub('[àáạảãâầấậẩẫăằắặẳẵ]', 'a', s)
    s = re.sub('[ÀÁẠẢÃĂẰẮẶẲẴÂẦẤẬẨẪ]', 'A', s)
    s = re.sub('[èéẹẻẽêềếệểễ]', 'e', s)
    s = re.sub('[ÈÉẸẺẼÊỀẾỆỂỄ]', 'E', s)
    s = re.sub('[òóọỏõôồốộổỗơờớợởỡ]', 'o', s)
    s = re.sub('[ÒÓỌỎÕÔỒỐỘỔỖƠỜỚỢỞỠ]', 'O', s)
    s = re.sub('[ìíịỉĩ]', 'i', s)
    s = re.sub('[ÌÍỊỈĨ]', 'I', s)
    s = re.sub('[ùúụủũưừứựửữ]', 'u', s)
    s = re.sub('[ƯỪỨỰỬỮÙÚỤỦŨ]', 'U', s)
    s = re.sub('[ỳýỵỷỹ]', 'y', s)
    s = re.sub('[ỲÝỴỶỸ]', 'Y', s)
    s = re.sub('[Đ]', 'D', s)
    s = re.sub('[đ]', 'd', s)
    return s.encode('utf-8')
