from rest_framework import serializers
from paper.models import PaperData


class PaperDataSerializer(serializers.ModelSerializer):
    class Meta:
        model = PaperData
        fields = ('id', 'title', 'contents')
