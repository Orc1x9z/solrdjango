import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'highlights'
})
export class HighlightSearch implements PipeTransform {

  transform(item: string, searchText: string[]): string {
    searchText.forEach(word => {
      if (word.length > 0) {
        const re = new RegExp(word, 'gi');
        item = item.replace(re, `<mark>${word}</mark>`);
      }
    });
    return item;

  }

}

