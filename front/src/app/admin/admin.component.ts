import { Component, OnInit } from '@angular/core';
import { PaperDataService } from '../paper-data.service';
import { Paper } from '../model/paper';
@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit {

  id: number = -1;
  title: string;
  contents: string;

  listPaper: Paper[];

  constructor(
    private paperDataService: PaperDataService
  ) { }

  ngOnInit() {
    this.id = -1;
    this.title = "";
    this.contents = "";
    this.paperDataService.getData().subscribe(
      data => {
        if (data && data.length > 0) {
          data.forEach(paper => {
            paper.contents = paper.contents.replace(/(\r\n|\n|\r)/gm,"<br>");
          });
          this.listPaper = data;
        }
      }
    )
  }

  findRecord(id){
    this.paperDataService.findData(id).subscribe(
      paper =>{
        if(paper && paper != null){
          this.id = paper.id;
          this.title = paper.title;
          this.contents = paper.contents;
        }
      }
    )
  }

  addOrEdit() {
    if (this.title && this.title.length > 0 &&
      this.contents && this.contents.length > 0) {
        if(this.id != -1){
          // EDIT record
          this.paperDataService.editPaper(this.id,this.title,this.contents).subscribe(
            paper => {
              alert("Edit success");
              this.ngOnInit();
            }
          )
        }else{
          // ADD new record
          this.paperDataService.addNewPaper(this.title, this.contents).subscribe(
            data => {
              alert("Add success");
              this.ngOnInit();
            },
            error => {
              if (error) {
                alert("Add failed")
                console.log(error);
              }
            }
          )
        }
      
    } else {
      alert("Must input all boxes");
    }
  }

  deleteRecord(id: number, title) {
    if (confirm("Are you sure want to delete " + title)) {
      this.paperDataService.deleteData(id).subscribe(___ => { })
      this.ngOnInit();
    }
  }
  cancelEdit(){
    this.ngOnInit();
  }
}
