import { TestBed } from '@angular/core/testing';

import { PaperDataService } from './paper-data.service';

describe('PaperDataService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PaperDataService = TestBed.get(PaperDataService);
    expect(service).toBeTruthy();
  });
});
