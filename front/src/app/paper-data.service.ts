import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http'
import { catchError, map, tap } from 'rxjs/operators';
import { environment } from '../environments/environment';
import { of, Observable } from 'rxjs';
import { Paper } from '../app/model/paper';

const httpHeaders = new HttpHeaders()
  .set('Content-Type', 'application/json')
  .set('Cache-Control', 'no-cache');

let options = {
  headers: httpHeaders
};

@Injectable({
  providedIn: 'root'
})

export class PaperDataService {

  dataUrl = 'http://127.0.0.1:8000/paper/';
  constructor(private http: HttpClient) { }

  addNewPaper(title, contents): Observable<any> {
    return this.http.post<any>(`${this.dataUrl}`,
      {
        "title": title,
        "contents": contents
      });
  }

  editPaper(id, title, contents): Observable<any> {
    return this.http.put<any>(`${this.dataUrl}${id}/`,
      {
        "title": title,
        "contents": contents
      });
  }

  findData(id): Observable<any> {
    return this.http.get<any[]>(`${this.dataUrl}${id}`).pipe(map(data => data));
  }

  getData(): Observable<any> {
    return this.http.get<any[]>(`${this.dataUrl}`).pipe(map(data => data));
  }

  deleteData(id): Observable<any> {
    return this.http.delete<any>(`${this.dataUrl}${id}`).pipe(
      catchError(error => of(new (Paper)))
    );
  }

  searchData(contents): Observable<any> {
    // return this.http.get<any[]>(`${this.dataUrl}search/${contents}/`).pipe(map(data => data));
    return this.http.post<any[]>(`${this.dataUrl}search/`,{
      "search_string": contents
    }).pipe(map(data => data));
  }
}
