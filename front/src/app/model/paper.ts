export class Paper {
    id: string;
    title: string;
    contents: string;
}