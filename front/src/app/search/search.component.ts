import { Component, OnInit } from '@angular/core';
import { Paper } from '../model/paper';
import { PaperDataService } from '../paper-data.service';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})

export class SearchComponent implements OnInit {
  textSearch: string;
  textHighlight: string[];

  listPaper: Paper[];
  constructor(
    private paperDataService: PaperDataService
  ) { }

  ngOnInit() {

  }



  search() {
    this.paperDataService.searchData(this.textSearch).subscribe(
      data => {
        console.log(data);
        
        data.forEach(paper => {
          paper.contents = paper.contents.replace(/(\r\n|\n|\r)/gm, "<br>");
        });
        this.listPaper = data;
      }

    )
  }

}
