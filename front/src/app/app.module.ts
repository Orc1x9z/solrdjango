import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent} from './app.component';
import { AdminComponent } from './admin/admin.component';
import { SearchComponent } from './search/search.component';
import { HighlightSearch } from './custom-pipe.pipe';

@NgModule({
  declarations: [
    AppComponent,
    AdminComponent,
    SearchComponent,
    HighlightSearch
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
